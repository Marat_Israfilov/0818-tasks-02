/*Программа, определяющая в двумерном массиве строку, имеющую наибольшее количество повторений некоторого значения.*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 10

void fill_array(int arr[][N], int num1, int num2) //заполняем массив случайными числами от num1 до num2
{
	int i, j;
	srand(time(0));
	for(i = 0; i < N; i++)
	{
		for(j = 0; j < N; j++)
		{
			arr[i][j] = num1 + rand() % (num2 - num1 + 1);
			printf("%d ", arr[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

int comp(const void *i, const void *j) //сортируем элементы по возрастанию в qsort
{
  return *(int *)i - *(int *)j;
}


int str_sequence_length(int arr[][N]) //Определяем строку с наибольшим количеством повторений занчения
{
	int i, j, count, max, str;
	count = 1;
	max = 0;
	for(i = 0; i < N; i++)
	{
		for(j = 1; j < N+1; j++)
		{
			if((arr[i][j] == arr[i][j-1]) && (j != N))
			{
				count++;
			}
			else if(count > max)
			{
				max = count;
				count = 1;
				str = i+1;
			}
			else
			{		
			count = 1;
			}
		}
	}
	return str;
}

int main()
{
	int array[N][N], i, j, num1, num2;

	printf("Enter a range of numbers(num1 num2):\n");
	scanf("%d %d", &num1, &num2);

	fill_array(array, num1, num2);

	for(i = 0; i < N; i++)
	{
		qsort(array[i], N, sizeof(int), comp);
	}
	
	for(i = 0; i < N; i++)
        {
                for(j = 0; j < N; j++)
                {            
                	printf("%d ", array[i][j]);
                }
                printf("\n");
        }

	printf("Max string array is %d\n", str_sequence_length(array));
	return 0;
}
